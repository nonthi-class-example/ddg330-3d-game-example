using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Footstep : MonoBehaviour
{
    [SerializeField] private int _currentTerrainIndex;

    private TerrainDetector _terrainDetector;

    private void Awake()
    {
        _terrainDetector = new TerrainDetector();
    }

    private void Update()
    {
        _currentTerrainIndex = _terrainDetector.GetActiveTerrainTextureIdx(transform.position);
    }
}
