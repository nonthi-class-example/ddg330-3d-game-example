using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoClipMovement : MonoBehaviour
{
    [SerializeField] private float _moveSpeed = 5f;

    [SerializeField] private float _cameraHorizontalSpeed = 720f;
    [SerializeField] private float _cameraVerticalSpeed = 360f;
    [SerializeField] private float _cameraMaxAngle = 60f;
    [SerializeField] private float _cameraMinAngle = 0f;
    [SerializeField] private bool _invertCameraVertical = true;

    private float _camHorizontalAngle = 0f;
    private float _camVerticalAngle = 15f;

    private void Update()
    {
        _camHorizontalAngle += Input.GetAxis("Mouse X") * _cameraHorizontalSpeed * Time.deltaTime;
        _camHorizontalAngle = (_camHorizontalAngle + 360f) % 360f;

        float vertMult = _invertCameraVertical ? -1f : 1f;
        _camVerticalAngle += Input.GetAxis("Mouse Y") * _cameraVerticalSpeed * Time.deltaTime * vertMult;
        _camVerticalAngle = Mathf.Clamp(_camVerticalAngle, _cameraMinAngle, _cameraMaxAngle);

        transform.rotation = Quaternion.Euler(_camVerticalAngle, _camHorizontalAngle, 0f);

        Vector3 moveDirection = new Vector3(Input.GetAxisRaw("Horizontal"), 0f, Input.GetAxisRaw("Vertical")).normalized;

        transform.position += transform.TransformDirection(moveDirection) * _moveSpeed * Time.deltaTime;
    }
}
