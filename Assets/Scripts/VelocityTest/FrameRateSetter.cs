using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrameRateSetter : MonoBehaviour
{
    [SerializeField] private int _targetFrameRate = -1;

    private void OnEnable()
    {
        Application.targetFrameRate = _targetFrameRate;
    }

    private void OnDisable()
    {
        Application.targetFrameRate = -1;
    }
}
