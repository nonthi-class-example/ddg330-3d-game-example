using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VelocityTestMovement : MonoBehaviour
{
    [SerializeField] private float _acceleration = 1f;

    private Rigidbody _rb;

    private float _speed;

    private void Awake()
    {
        TryGetComponent(out _rb);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        _speed += _acceleration * Time.deltaTime;
        _rb.velocity = transform.right * _speed;
    }
}
