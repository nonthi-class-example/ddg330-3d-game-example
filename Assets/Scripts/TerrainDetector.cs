using UnityEngine;

public class TerrainDetector
{
    private TerrainData _terrainData;
    private int _alphaMapWidth;
    private int _alphaMapHeight;
    private float[,,] _splatMapData;
    private int _numTextures;

    public TerrainDetector()
    {
        _terrainData = Terrain.activeTerrain.terrainData;
        _alphaMapWidth = _terrainData.alphamapWidth;
        _alphaMapHeight = _terrainData.alphamapHeight;

        _splatMapData = _terrainData.GetAlphamaps(0, 0, _alphaMapWidth, _alphaMapHeight);
        _numTextures = _splatMapData.Length / (_alphaMapWidth * _alphaMapHeight);
    }

    private Vector3 ConvertToSplatMapCoordinate(Vector3 worldPosition)
    {
        Vector3 splatPosition = new Vector3();
        Terrain ter = Terrain.activeTerrain;
        Vector3 terPosition = ter.transform.position;
        splatPosition.x = ((worldPosition.x - terPosition.x) / ter.terrainData.size.x) * ter.terrainData.alphamapWidth;
        splatPosition.z = ((worldPosition.z - terPosition.z) / ter.terrainData.size.z) * ter.terrainData.alphamapHeight;
        return splatPosition;
    }

    public int GetActiveTerrainTextureIdx(Vector3 position)
    {
        Vector3 terrainCord = ConvertToSplatMapCoordinate(position);
        int activeTerrainIndex = 0;
        float largestOpacity = 0f;

        for (int i = 0; i < _numTextures; i++)
        {
            if (largestOpacity < _splatMapData[(int)terrainCord.z, (int)terrainCord.x, i])
            {
                activeTerrainIndex = i;
                largestOpacity = _splatMapData[(int)terrainCord.z, (int)terrainCord.x, i];
            }
        }

        return activeTerrainIndex;
    }

}