using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ToastNotificationDisplay : MonoBehaviour
{
    [SerializeField] private TMP_Text _textMesh;
    [SerializeField] private RectTransform _toastRectTransform;

    [SerializeField] private float _displayDuration = 3f;

    [SerializeField] private float _animSpeed = 5f;

    [SerializeField] private float _yOffsetHide = 0f;
    [SerializeField] private float _yOffsetShow = -32f;

    [SerializeField] private float _yPivotHide = 0f;
    [SerializeField] private float _yPivotShow = 1f;

    private bool _isShowing;
    private float _t;

    private float _displayTimer;

    public bool IsIdle => !_isShowing && _t == 0f;

    private void Update()
    {
        if (_isShowing && _t >= 1f)
        {
            _displayTimer = Mathf.MoveTowards(_displayTimer, 0f, Time.deltaTime);
            if (_displayTimer <= 0f)
            {
                _isShowing = false;
            }
        }

        _t = Mathf.MoveTowards(_t, _isShowing ? 1f : 0f, Time.deltaTime * _animSpeed);

        Vector2 pos = _toastRectTransform.anchoredPosition;
        pos.y = Mathf.Lerp(_yOffsetHide, _yOffsetShow, _t);
        _toastRectTransform.anchoredPosition = pos;

        Vector2 pivot = _toastRectTransform.pivot;
        pivot.y = Mathf.Lerp(_yPivotHide, _yPivotShow, _t);
        _toastRectTransform.pivot = pivot;
    }

    public void ShowNotification(string text)
    {
        _textMesh.text = text;
        _isShowing = true;
        _displayTimer = _displayDuration;
    }
}
