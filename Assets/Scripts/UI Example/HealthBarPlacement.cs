using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBarPlacement : MonoBehaviour
{
    [SerializeField] private Transform _anchorRef;
    [SerializeField] private float _offset = 0.5f;
    [SerializeField] private Camera _camera;

    private void Update()
    {
        AdjustPosition();
    }

    private void AdjustPosition()
    {
        transform.position = _anchorRef.position + (_offset * _camera.transform.up);
        transform.rotation = _camera.transform.rotation;
    }
}
