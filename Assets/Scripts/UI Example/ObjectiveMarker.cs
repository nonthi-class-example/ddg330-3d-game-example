using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ObjectiveMarker : MonoBehaviour
{
    [SerializeField] private Transform _playerTransform;
    [SerializeField] private Camera _camera;
    [SerializeField] private RectTransform _markerRectTransform;
    [SerializeField] private Transform _objectiveTransform;
    [SerializeField] private RectTransform _arrowTransform;
    [SerializeField] private float _arrowArmLength = 40f;
    [SerializeField] private TMP_Text _distanceText;

    [Range(0.01f, 0.5f)]
    [SerializeField] private float _radius = 0.4f;

    private void Update()
    {
        Vector3 objPos = _objectiveTransform.position;
        bool behind = false;

        Vector3 camToObj = objPos - _camera.transform.position;

        if (Vector3.Dot(camToObj, _camera.transform.forward) < 0f)
        {
            objPos += 2 * Vector3.Project(-camToObj, _camera.transform.forward);
            behind = true;
        }

        Vector2 screenPos = _camera.WorldToViewportPoint(objPos);

        Vector2 fromCenter = screenPos - new Vector2(0.5f, 0.5f);
        if (fromCenter.magnitude > _radius || behind)
        {
            Vector3 fromCenterNormalized = fromCenter.normalized;
            fromCenter = fromCenterNormalized * _radius;

            _arrowTransform.gameObject.SetActive(true);
            Vector2 arrowDir = new Vector2(fromCenterNormalized.x * _camera.pixelWidth, fromCenterNormalized.y * _camera.pixelHeight).normalized;
            _arrowTransform.anchoredPosition = arrowDir * _arrowArmLength;
            _arrowTransform.localRotation = Quaternion.LookRotation(Vector3.forward, arrowDir);

            _distanceText.gameObject.SetActive(false);
        }
        else
        {
            _arrowTransform.gameObject.SetActive(false);
            _distanceText.gameObject.SetActive(true);
            //string formattedDist = string.Format("{0:F1}", Vector3.Distance(_playerTransform.position, _objectiveTransform.position));
            string formattedDist = "" + (int)Mathf.Round(Vector3.Distance(_playerTransform.position, _objectiveTransform.position));
            _distanceText.text = $"{formattedDist} m";
        }

        screenPos = new Vector2(0.5f, 0.5f) + fromCenter;

        _markerRectTransform.anchorMin = screenPos;
        _markerRectTransform.anchorMax = screenPos;
    }
}
