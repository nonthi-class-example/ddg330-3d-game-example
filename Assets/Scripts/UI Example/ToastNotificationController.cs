using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToastNotificationController : MonoBehaviour
{
    [SerializeField] private ToastNotificationDisplay _display;

    private Queue<string> _notificationQueue = new Queue<string>();

    private void Update()
    {
        if (_notificationQueue.Count > 0 && _display.IsIdle)
        {
            _display.ShowNotification(_notificationQueue.Dequeue());
        }
    }

    public void QueueToastNotification(string text)
    {
        _notificationQueue.Enqueue(text);
    }
}
