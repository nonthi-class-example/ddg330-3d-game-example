using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour
{
    [SerializeField] private CharacterHealth _characterHealth;

    [SerializeField] private RectTransform _hpRect;
    [SerializeField] private RectTransform _healingRect;
    [SerializeField] private RectTransform _damageRect;

    [SerializeField] private float _continuousChangeInterval = 0.1f;
    [SerializeField] private float _changeDrainDelay = 1f;
    [SerializeField] private float _changeDrainRate = 200f;

    private List<HealthBarChangeEntry> _changeEntries = new List<HealthBarChangeEntry>();

    private float _maxHealth;
    private float _currentHealth;

    private float _continuousDamageTimer;
    private float _continuousHealTimer;

    private HealthBarChangeEntry _lastDamageEntry;
    private HealthBarChangeEntry _lastHealEntry;

    private void Start()
    {
        _maxHealth = _characterHealth.MaxHealth;
        _currentHealth = _characterHealth.CurrentHealth;

        _changeEntries.Clear();
    }

    private void OnEnable()
    {
        _characterHealth.OnHealthChange += HandleHealthChanged;
    }

    private void OnDisable()
    {
        _characterHealth.OnHealthChange -= HandleHealthChanged;
    }

    // Update is called once per frame
    void Update()
    {
        UpdateDisplay();
    }

    private void UpdateDisplay()
    {
        _continuousDamageTimer = Mathf.MoveTowards(_continuousDamageTimer, 0f, Time.deltaTime);
        _continuousHealTimer = Mathf.MoveTowards(_continuousHealTimer, 0f, Time.deltaTime);
        if (_continuousDamageTimer <= 0f)
        {
            _lastDamageEntry = null;
        }
        if (_continuousHealTimer <= 0f)
        {
            _lastHealEntry = null;
        }

        float totalHealing = 0f;
        float totalDamage = 0f;

        foreach (HealthBarChangeEntry entry in _changeEntries)
        {
            entry.DrainDelayTimer = Mathf.MoveTowards(entry.DrainDelayTimer, 0f, Time.deltaTime);
            if (entry.DrainDelayTimer <= 0f)
            {
                entry.Delta = Mathf.MoveTowards(entry.Delta, 0f, _changeDrainRate * Time.deltaTime);
            }

            if (entry.Delta < 0f)
            {
                totalDamage -= entry.Delta;
            }
            else
            {
                totalHealing += entry.Delta;
            }
        }

        SetRectAnchorX(_hpRect, (_currentHealth - totalHealing) / _maxHealth);
        SetRectAnchorX(_healingRect, _currentHealth / _maxHealth);
        SetRectAnchorX(_damageRect, (_currentHealth + totalDamage) / _maxHealth);

        _changeEntries.RemoveAll(x => Mathf.Approximately(x.Delta, 0f));
    }

    private void SetRectAnchorX(RectTransform rect, float x)
    {
        Vector2 anchor = rect.anchorMax;
        anchor.x = Mathf.Clamp01(x);
        rect.anchorMax = anchor;
    }

    public void HandleHealthChanged(float newCurrentHealth)
    {
        float delta = newCurrentHealth - _currentHealth;
        _currentHealth = newCurrentHealth;

        if (Mathf.Approximately(delta, 0f)) return;

        HealthBarChangeEntry currentEntry = null;

        float counterPoint = delta;

        if (delta > 0f)
        {
            if (_lastHealEntry != null && !Mathf.Approximately(_lastHealEntry.Delta, 0f))
            {
                currentEntry = _lastHealEntry;
            }
            else
            {
                currentEntry = new HealthBarChangeEntry();
                _continuousHealTimer = _continuousChangeInterval;
                _lastHealEntry = currentEntry;
                _changeEntries.Add(currentEntry);
                currentEntry.DrainDelayTimer = _changeDrainDelay;
            }

            foreach (var existingEntry in _changeEntries)
            {
                if (counterPoint <= 0f) break;

                if (existingEntry.Delta < 0f)
                {
                    float valueUpdated = Mathf.Min(counterPoint, -existingEntry.Delta);
                    existingEntry.Delta += valueUpdated;
                    counterPoint -= valueUpdated;
                }
            }
        }
        else if (delta < 0f)
        {
            if (_lastDamageEntry != null && !Mathf.Approximately(_lastDamageEntry.Delta, 0f))
            {
                currentEntry = _lastDamageEntry;
            }
            else
            {
                currentEntry = new HealthBarChangeEntry();
                _continuousDamageTimer = _continuousChangeInterval;
                _lastDamageEntry = currentEntry;
                _changeEntries.Add(currentEntry);
                currentEntry.DrainDelayTimer = _changeDrainDelay;
            }

            foreach (var existingEntry in _changeEntries)
            {
                if (counterPoint >= 0f) break;

                if (existingEntry.Delta > 0f)
                {
                    float valueUpdated = Mathf.Min(-counterPoint, existingEntry.Delta);
                    existingEntry.Delta -= valueUpdated;
                    counterPoint += valueUpdated;
                }
            }
        }

        currentEntry.Delta += delta;
    }
}

[System.Serializable]
public class HealthBarChangeEntry
{
    public float Delta;
    public float DrainDelayTimer;
}
