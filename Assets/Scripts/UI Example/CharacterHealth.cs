using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterHealth : MonoBehaviour
{
    [SerializeField] private float _maxHealth = 100f;
    public float MaxHealth => _maxHealth;

    private float _currentHealth;
    public float CurrentHealth => _currentHealth;

    public delegate void HealthChangeEvent(float currentHealth);
    public event HealthChangeEvent OnHealthChange;

    [Header("Debug")]
    [SerializeField] private float _debugValue = 10f;
    [SerializeField] private KeyCode _debugDamageKey = KeyCode.E;
    [SerializeField] private KeyCode _debugHealKey = KeyCode.Q;

    private void Awake()
    {
        _currentHealth = _maxHealth;
    }

    private void Update()
    {
        if (Input.GetKeyDown(_debugDamageKey))
        {
            ChangeHealth(-_debugValue);
        }
        if (Input.GetKeyDown(_debugHealKey))
        {
            ChangeHealth(_debugValue);
        }
    }

    public void ChangeHealth(float delta)
    {
        float prevHealth = _currentHealth;
        _currentHealth = Mathf.Clamp(_currentHealth + delta, 0f, _maxHealth);
        delta = _currentHealth - prevHealth;

        OnHealthChange?.Invoke(_currentHealth);
    }
}
