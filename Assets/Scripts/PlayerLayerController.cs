using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class PlayerLayerController : MonoBehaviour
{
    [SerializeField][Layer] private int _silhouetteLayer;

    private List<Transform> _transformsToChange = new List<Transform>();

    private void Start()
    {
        _transformsToChange = transform.Cast<Transform>().ToList();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.K))
        {
            foreach (Transform t in _transformsToChange)
            {
                t.gameObject.layer = _silhouetteLayer;
            }
        }
    }
}
