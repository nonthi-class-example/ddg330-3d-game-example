using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimationControllerBlend2D : MonoBehaviour
{
    [SerializeField] private float _acceleration = 1f;
    [SerializeField] private float _deceleration = 2f;

    [SerializeField] private float _walkSpeed = 0.5f;
    [SerializeField] private float _runSpeed = 2f;

    private Animator _animator;
    private int _velocityZHash;
    private int _velocityXHash;

    private float _velocityZ = 0f;
    private float _velocityX = 0f;

    // Start is called before the first frame update
    void Awake()
    {
        TryGetComponent(out _animator);
        _velocityZHash = Animator.StringToHash("velocityZ");
        _velocityXHash = Animator.StringToHash("velocityX");
    }

    // Update is called once per frame
    void Update()
    {
        bool forwardPressed = Input.GetKey(KeyCode.W);
        bool rightPressed = Input.GetKey(KeyCode.D);
        bool leftPressed = Input.GetKey(KeyCode.A);
        bool runPressed = Input.GetKey(KeyCode.LeftShift);

        float maxSpeed = runPressed ? _runSpeed : _walkSpeed;
        float sideDirMult = 0f;
        if (rightPressed)
        {
            sideDirMult += 1f;
        }
        if (leftPressed)
        {
            sideDirMult -= 1f;
        }

        float targetForwardSpeed = forwardPressed ? maxSpeed : 0f;

        float targetSideSpeed = maxSpeed * sideDirMult;
        if (targetSideSpeed * _velocityX < 0f || Mathf.Abs(_velocityX) > Mathf.Abs(targetSideSpeed))
        {
            _velocityX = Mathf.MoveTowards(_velocityX, 0f, Time.deltaTime * _deceleration);
        }
        else
        {
            _velocityX = Mathf.MoveTowards(_velocityX, targetSideSpeed, Time.deltaTime * _acceleration);
        }

        if (Mathf.Abs(_velocityZ) > Mathf.Abs(targetForwardSpeed))
        {
            _velocityZ = Mathf.MoveTowards(_velocityZ, 0f, Time.deltaTime * _deceleration);
        }
        else
        {
            _velocityZ = Mathf.MoveTowards(_velocityZ, targetForwardSpeed, Time.deltaTime * _acceleration);
        }

        _animator.SetFloat(_velocityZHash, _velocityZ);
        _animator.SetFloat(_velocityXHash, _velocityX);
    }
}
