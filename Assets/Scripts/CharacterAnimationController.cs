using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimationController : MonoBehaviour
{
    [SerializeField] private float _acceleration = 1f;
    [SerializeField] private float _deceleration = 2f;

    private Animator _animator;
    private int _speedHash;

    private float _speed = 0f;

    // Start is called before the first frame update
    void Start()
    {
        TryGetComponent(out _animator);
        _speedHash = Animator.StringToHash("velocityZ");
    }

    // Update is called once per frame
    void Update()
    {
        bool isWalking = Input.GetKey(KeyCode.W);
        if (isWalking)
        {
            _speed = Mathf.MoveTowards(_speed, 1f, Time.deltaTime * _acceleration);
        }
        else
        {
            _speed = Mathf.MoveTowards(_speed, 0f, Time.deltaTime * _deceleration);
        }

        _animator.SetFloat(_speedHash, _speed);
    }
}
