using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class TestNavigation : MonoBehaviour
{
    [SerializeField] private Transform _target;

    private NavMeshAgent _navAgent;

    private void Awake()
    {
        TryGetComponent(out _navAgent);
    }

    // Update is called once per frame
    void Update()
    {
        _navAgent.destination = _target.position;
    }
}
