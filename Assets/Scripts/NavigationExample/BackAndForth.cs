using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackAndForth : MonoBehaviour
{
    [SerializeField] private Transform _startMarker;
    [SerializeField] private Transform _endMarker;

    [SerializeField] private float _speed = 1f;

    private float _timer;

    // Update is called once per frame
    void Update()
    {
        _timer += _speed * Time.deltaTime;
        _timer %= 2f;

        transform.position = Vector3.Lerp(_startMarker.position, _endMarker.position, Mathf.PingPong(_timer, 1f));
    }
}
