using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyNavigation : MonoBehaviour
{
    [SerializeField] private Transform _target;
    [SerializeField] private GeneralCharacterController _charController;
    [SerializeField] private float _runThreshold = 5f;
    [SerializeField] private float _stopRange = 2f;
    private NavMeshAgent _navAgent;

    private bool _isManualMove;

    [SerializeField] private bool _onLink;

    private void Awake()
    {
        TryGetComponent(out _navAgent);
    }

    private void Start()
    {
        EnableManualMove();
    }

    // Update is called once per frame
    void Update()
    {
        _navAgent.destination = _target.position;
        Move();
    }

    private void Move()
    {
        _onLink = _navAgent.isOnOffMeshLink;

        if (_isManualMove && _navAgent.isOnOffMeshLink)
        {
            DisableManualMove();
        }
        if (!_isManualMove && !_navAgent.isOnOffMeshLink)
        {
            EnableManualMove();
        }

        if (_isManualMove)
        {
            float forward = 0f;
            float side = 0f;

            if ((_target.position - transform.position).magnitude > _stopRange)
            {
                forward = Vector3.Dot(_navAgent.desiredVelocity, transform.forward);
                side = Vector3.Dot(_navAgent.desiredVelocity, transform.right);

                if (!Mathf.Approximately(forward, 0f))
                {
                    forward = Mathf.Sign(forward);
                }
                if (!Mathf.Approximately(side, 0f))
                {
                    side = Mathf.Sign(side);
                }
            }

            _charController.SetFrontMoveIntent(forward);
            _charController.SetSideMoveIntent(side);

            _charController.SetRunIntent(_navAgent.remainingDistance > _runThreshold);

            if (_navAgent.desiredVelocity.magnitude > 0f)
            {
                _charController.SetLookIntent(_navAgent.desiredVelocity.normalized);
            }

            _navAgent.nextPosition = transform.position;
        }
    }

    private void EnableManualMove()
    {
        _isManualMove = true;
        _navAgent.updatePosition = false;
        _navAgent.updateRotation = false;

        //_charController.Body.isKinematic = false;
    }

    private void DisableManualMove()
    {
        _isManualMove = false;

        _navAgent.updatePosition = true;
        _navAgent.updateRotation = true;

        //_charController.Body.isKinematic = true;
    }
}
