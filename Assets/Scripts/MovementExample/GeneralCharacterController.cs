using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneralCharacterController : MonoBehaviour
{
    [SerializeField] private GroundCheck _groundCheck;

    [Header("Basic Movement")]
    [SerializeField] private float _rotateSpeed = 360f;

    [SerializeField] private float _acceleration = 1f;
    [SerializeField] private float _deceleration = 2f;

    [SerializeField] private float _walkSpeed = 2f;
    public float WalkSpeed => _walkSpeed;

    [SerializeField] private float _runSpeed = 5f;
    public float RunSpeed => _runSpeed;

    [SerializeField] private float _walkBackSpeed = 1f;

    [SerializeField] private float _slopeCheckRange = 0.2f;
    [SerializeField] private float _slideSpeed = 6f;

    [SerializeField] private float _airMult = 0.5f;
    [SerializeField] private float _gravityScale = 3f;

    [SerializeField] private float _jumpPower = 5f;
    [SerializeField] private float _jumpClearance = 0.1f;

    private float _intentVelocityZ = 0f;
    public float IntentVelocityZ => _intentVelocityZ;

    private float _intentVelocityX = 0f;
    public float IntentVelocityX => _intentVelocityX;

    private CharacterController _cc;
    private Vector3 _ccVelocity;

    private Vector3 _ccHitNormal;
    public bool IsGrounded => _groundCheck.IsGrounded && _jumpClearanceTimer <= 0f;

    

    private float _frontMoveIntent;
    private float _sideMoveIntent;
    private Vector3 _lookIntent;
    private bool _runIntent;

    private bool _jumpRequested;
    private float _jumpClearanceTimer = 0f;

    // Start is called before the first frame update
    void Awake()
    {
        TryGetComponent(out _cc);
    }

    private void FixedUpdate()
    {
        NormalMovement();
        ProcessJump();

        if (_jumpClearanceTimer > 0f)
        {
            _jumpClearanceTimer = Mathf.MoveTowards(_jumpClearanceTimer, 0f, Time.deltaTime);
        }

        _ccVelocity.y += Physics.gravity.y * _gravityScale * Time.deltaTime;

        if (!IsGrounded)
        {
            _ccVelocity.x += (1f - _ccHitNormal.y) * _ccHitNormal.x * _slideSpeed;
            _ccVelocity.z += (1f - _ccHitNormal.y) * _ccHitNormal.z * _slideSpeed;
        }

        _ccHitNormal = Vector3.up;
        _cc.Move(_ccVelocity * Time.deltaTime);
    }

    private void NormalMovement()
    {
        float acc = _acceleration;
        float dec = _deceleration;

        float maxSpeed = _runIntent ? _runSpeed : _walkSpeed;

        if (_frontMoveIntent < 0f)
        {
            maxSpeed = _walkBackSpeed;
        }

        if (!IsGrounded)
        {
            acc *= _airMult;
        }

        Vector3 targetVelocity = new Vector3(_sideMoveIntent, 0f, _frontMoveIntent);
        targetVelocity = targetVelocity.normalized * maxSpeed;

        if (targetVelocity.x * _intentVelocityX < 0f || Mathf.Abs(_intentVelocityX) > Mathf.Abs(targetVelocity.x))
        {
            _intentVelocityX = Mathf.MoveTowards(_intentVelocityX, 0f, Time.deltaTime * dec);
        }
        else
        {
            _intentVelocityX = Mathf.MoveTowards(_intentVelocityX, targetVelocity.x, Time.deltaTime * acc);
        }

        if (targetVelocity.z * _intentVelocityZ < 0f || Mathf.Abs(_intentVelocityZ) > Mathf.Abs(targetVelocity.z))
        {
            _intentVelocityZ = Mathf.MoveTowards(_intentVelocityZ, 0f, Time.deltaTime * dec);
        }
        else
        {
            _intentVelocityZ = Mathf.MoveTowards(_intentVelocityZ, targetVelocity.z, Time.deltaTime * acc);
        }

        Vector3 forward = transform.forward;
        if (_lookIntent.magnitude > 0f)
        {
            forward = Vector3.ProjectOnPlane(_lookIntent, _cc.transform.up);
        }
        Quaternion targetRotation = Quaternion.LookRotation(forward, _cc.transform.up);

        if (!Mathf.Approximately(_intentVelocityX, 0f) || !Mathf.Approximately(_intentVelocityZ, 0f))
        {
            _cc.transform.rotation = Quaternion.RotateTowards(_cc.transform.rotation, targetRotation, _rotateSpeed * Time.deltaTime);
        }

        _ccVelocity = targetRotation * new Vector3(_intentVelocityX, _ccVelocity.y, _intentVelocityZ);
        if (IsGrounded)
        {
            _ccVelocity.y = 0f;
            _ccVelocity = AdjustVelocityToSlope(_ccVelocity);
        }
    }

    private Vector3 AdjustVelocityToSlope(Vector3 velocity)
    {
        if (Physics.Raycast(transform.position, Vector3.down, out RaycastHit hit, _slopeCheckRange, _groundCheck.GroundLayerMask))
        {
            Quaternion slopeRotation = Quaternion.FromToRotation(Vector3.up, hit.normal);
            Vector3 adjustedVelocity = slopeRotation * velocity;

            if (adjustedVelocity.y < 0f)
            {
                return adjustedVelocity;
            }
        }

        return velocity;
    }

    private bool CapsuleCast(Vector3 direction, out RaycastHit hit, float maxDistance, int layerMask)
    {
        Vector3 p1 = _cc.transform.position + _cc.center + _cc.transform.up * -_cc.height * 0.5F;
        Vector3 p2 = p1 + _cc.transform.up * _cc.height;

        return Physics.CapsuleCast(p1, p2, _cc.radius, direction, out hit, maxDistance, layerMask);
    }

    private void ProcessJump()
    {
        if (!_jumpRequested) return;
        _jumpRequested = false;

        if (!IsGrounded) return;

        _ccVelocity = _jumpPower * _cc.transform.up + Vector3.ProjectOnPlane(_ccVelocity, _cc.transform.up);
        _jumpClearanceTimer = _jumpClearance;
    }

    public void SetFrontMoveIntent(float intent)
    {
        _frontMoveIntent = intent;
    }

    public void SetSideMoveIntent(float intent)
    {
        _sideMoveIntent = intent;
    }

    public void SetLookIntent(Vector3 intent)
    {
        _lookIntent = intent;
    }

    public void SetRunIntent(bool intent)
    {
        _runIntent = intent;
    }

    public void RequestJump()
    {
        _jumpRequested = true;
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        _ccHitNormal = hit.normal;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, transform.position + (_slopeCheckRange * Vector3.down));
    }
}
