using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    [SerializeField] private GeneralCharacterController _charController;
    [SerializeField] private Transform _camRef;

    private void Update()
    {
        bool forwardPressed = Input.GetKey(KeyCode.W);
        bool backwardPressed = Input.GetKey(KeyCode.S);
        bool rightPressed = Input.GetKey(KeyCode.D);
        bool leftPressed = Input.GetKey(KeyCode.A);
        bool runPressed = Input.GetKey(KeyCode.LeftShift);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            _charController.RequestJump();
        }

        float frontDirMult = 0f;
        if (forwardPressed)
        {
            frontDirMult += 1f;
        }
        if (backwardPressed)
        {
            frontDirMult -= 1f;
        }

        float sideDirMult = 0f;
        if (rightPressed)
        {
            sideDirMult += 1f;
        }
        if (leftPressed)
        {
            sideDirMult -= 1f;
        }

        _charController.SetFrontMoveIntent(frontDirMult);
        _charController.SetSideMoveIntent(sideDirMult);
        _charController.SetRunIntent(runPressed);
        _charController.SetLookIntent(_camRef.forward);
    }
}
