using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : MonoBehaviour
{
    [SerializeField] private float _speedParamMax = 2f;

    private Animator _animator;
    private GeneralCharacterController _movement;

    private int _velocityZHash;
    private int _velocityXHash;
    private int _isGroundedHash;

    private void Awake()
    {
        TryGetComponent(out _animator);
        TryGetComponent(out _movement);

        _velocityZHash = Animator.StringToHash("velocityZ");
        _velocityXHash = Animator.StringToHash("velocityX");

        _isGroundedHash = Animator.StringToHash("isGrounded");
    }

    private void Update()
    {
        _animator.SetFloat(_velocityZHash, (_movement.IntentVelocityZ / _movement.RunSpeed) * _speedParamMax);
        _animator.SetFloat(_velocityXHash, (_movement.IntentVelocityX / _movement.RunSpeed) * _speedParamMax);

        _animator.SetBool(_isGroundedHash, _movement.IsGrounded);
    }
}
