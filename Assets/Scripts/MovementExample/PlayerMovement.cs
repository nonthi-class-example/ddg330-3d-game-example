using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private Transform _camRef;
    [SerializeField] private float _rotateSpeed = 360f;

    [SerializeField] private float _acceleration = 1f;
    [SerializeField] private float _deceleration = 2f;

    [SerializeField] private float _walkSpeed = 2f;
    public float WalkSpeed => _walkSpeed;

    [SerializeField] private float _runSpeed = 5f;
    public float RunSpeed => _runSpeed;

    [SerializeField] private float _walkBackSpeed = 1f;

    private float _velocityZ = 0f;
    public float VelocityZ => _velocityZ;

    private float _velocityX = 0f;
    public float VelocityX => _velocityX;

    private Rigidbody _rb;

    // Start is called before the first frame update
    void Awake()
    {
        TryGetComponent(out _rb);
    }

    // Update is called once per frame
    void Update()
    {
        bool forwardPressed = Input.GetKey(KeyCode.W);
        bool backwardPressed = Input.GetKey(KeyCode.S);
        bool rightPressed = Input.GetKey(KeyCode.D);
        bool leftPressed = Input.GetKey(KeyCode.A);
        bool runPressed = Input.GetKey(KeyCode.LeftShift);

        float maxSpeed = runPressed ? _runSpeed : _walkSpeed;

        float frontDirMult = 0f;
        if (forwardPressed)
        {
            frontDirMult += 1f;
        }
        if (backwardPressed)
        {
            frontDirMult -= 1f;
            maxSpeed = _walkBackSpeed;
        }

        float sideDirMult = 0f;
        if (rightPressed)
        {
            sideDirMult += 1f;
        }
        if (leftPressed)
        {
            sideDirMult -= 1f;
        }

        Vector3 targetVelocity = new Vector3(sideDirMult, 0f, frontDirMult);
        targetVelocity = targetVelocity.normalized * maxSpeed;

        if (targetVelocity.x * _velocityX < 0f || Mathf.Abs(_velocityX) > Mathf.Abs(targetVelocity.x))
        {
            _velocityX = Mathf.MoveTowards(_velocityX, 0f, Time.deltaTime * _deceleration);
        }
        else
        {
            _velocityX = Mathf.MoveTowards(_velocityX, targetVelocity.x, Time.deltaTime * _acceleration);
        }

        if (targetVelocity.z * _velocityZ < 0f || Mathf.Abs(_velocityZ) > Mathf.Abs(targetVelocity.z))
        {
            _velocityZ = Mathf.MoveTowards(_velocityZ, 0f, Time.deltaTime * _deceleration);
        }
        else
        {
            _velocityZ = Mathf.MoveTowards(_velocityZ, targetVelocity.z, Time.deltaTime * _acceleration);
        }
    }

    private void FixedUpdate()
    {
        Vector3 forward = Vector3.ProjectOnPlane(_camRef.forward, _rb.transform.up);
        Quaternion targetRotation = Quaternion.LookRotation(forward, _rb.transform.up);

        if (!Mathf.Approximately(_velocityX, 0f) || !Mathf.Approximately(_velocityZ, 0f))
        {
            _rb.transform.rotation = Quaternion.RotateTowards(_rb.transform.rotation, targetRotation, _rotateSpeed * Time.deltaTime);
        }

        _rb.velocity = targetRotation * new Vector3(_velocityX, _rb.velocity.y, _velocityZ);
    }
}
