using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundCheck : MonoBehaviour
{
    [SerializeField] private float _checkRadius = 0.5f;
    [SerializeField] private LayerMask _groundLayerMask;
    public LayerMask GroundLayerMask => _groundLayerMask;

    private bool _isGrounded;
    public bool IsGrounded => _isGrounded;

    private void FixedUpdate()
    {
        _isGrounded = Physics.CheckSphere(transform.position, _checkRadius, _groundLayerMask);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, _checkRadius);
    }
}
