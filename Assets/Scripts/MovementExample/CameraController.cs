using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private Transform _target;
    [SerializeField] private Vector3 _positionOffset;

    [SerializeField] private float _cameraHorizontalSpeed = 720f;
    [SerializeField] private float _cameraVerticalSpeed = 360f;
    [SerializeField] private float _cameraMaxAngle = 60f;
    [SerializeField] private float _cameraMinAngle = 0f;
    [SerializeField] private bool _invertCameraVertical = true;
    [SerializeField] private float _cameraArmLength = 3f;
    [SerializeField] private float _cameraMinArmLength = 0.6f;
    [SerializeField] private float _sphereCastRadius = 0.3f;

    [SerializeField] private LayerMask _cameraObstacleLayerMask;

    private float _camHorizontalAngle = 0f;
    private float _camVerticalAngle = 15f;

    private RaycastHit _camObstacleHit;

    private void Start()
    {
    }

    void Update()
    {
        bool canRotate = true;

        if (Input.GetKey(KeyCode.LeftAlt))
        {
            Cursor.lockState = CursorLockMode.None;
            canRotate = false;
        }
        else
        {
            Cursor.lockState = CursorLockMode.Locked;
        }

        if (canRotate)
        {
            _camHorizontalAngle += Input.GetAxis("Mouse X") * _cameraHorizontalSpeed * Time.deltaTime;
            _camHorizontalAngle = (_camHorizontalAngle + 360f) % 360f;

            float vertMult = _invertCameraVertical ? -1f : 1f;
            _camVerticalAngle += Input.GetAxis("Mouse Y") * _cameraVerticalSpeed * Time.deltaTime * vertMult;
            _camVerticalAngle = Mathf.Clamp(_camVerticalAngle, _cameraMinAngle, _cameraMaxAngle);

            transform.rotation = Quaternion.Euler(_camVerticalAngle, _camHorizontalAngle, 0f);
        }

        Vector3 refPos = _target.position + _positionOffset;

        float distance = _cameraArmLength;

        if (Physics.SphereCast(refPos, _sphereCastRadius, -transform.forward, out _camObstacleHit, _cameraArmLength, _cameraObstacleLayerMask))
        {
            distance = Mathf.Max(_camObstacleHit.distance, _cameraMinArmLength);
        }

        transform.position = refPos - (transform.forward * distance);
    }
}
