using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAim : MonoBehaviour
{
    [SerializeField] private int _aimLayer = 1;
    [SerializeField] private float _aimSpeed = 2f;

    private Animator _animator;

    private float _aimWeight = 0f;

    private void Awake()
    {
        TryGetComponent(out _animator);
    }

    private void Update()
    {
        float _aimTarget = Input.GetKey(KeyCode.Mouse1) ? 1f : 0f;
        _aimWeight = Mathf.MoveTowards(_aimWeight, _aimTarget, _aimSpeed * Time.deltaTime);

        _animator.SetLayerWeight(_aimLayer, _aimWeight);
    }
}
